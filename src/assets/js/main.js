(function($, window, document) {
    $(function() {

        $('.hamburger--slider').on('click', function(e){
            $(this).toggleClass('is-active');
            $(this).next('div').toggleClass('show');
        });

        const $navigation = $("#navigation");

        $navigation.navigation({
            mobileBreakpoint: 975
        });

        $navigation.fixed({
            topSpace: 0,
        });

        new Swiper('.swiper-container-news', {
            effect: 'fade',
            navigation: {
                nextEl: '.button-next',
                prevEl: '.button-prev',
            },
        });

        new Swiper('.swiper-container-first', {
            effect: 'fade',
            navigation: {
                nextEl: '.button-next',
                prevEl: '.button-prev',
            },
        });

    });
}(window.jQuery, window, document));
